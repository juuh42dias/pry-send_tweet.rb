def run(*cmd)
  Process.wait Kernel.spawn(*cmd)
  if !$?.success?
    raise cmd.join(' ') + " failed."
  end
end

ARGV.each do |argument|
  case argument
  when '--fresh' then run('vagrant', 'destroy', '--force')
  end
end

run 'vagrant', 'up'
run 'vagrant', 'ssh', '-c', 'tmuxinator start pry_send_tweet'

Kernel.require_relative 'lib/pry/send_tweet/version'
Gem::Specification.new do |spec|
  spec.name = "pry-send_tweet.rb"
  spec.version = Pry::SendTweet::VERSION
  spec.authors = ["Pry developers", "Fry developers", "trebor8"]
  spec.email = "trebor.g@protonmail.com"
  spec.summary = "An extensible Twitter client living inside the Pry and Fry REPLs."
  spec.description = spec.summary
  spec.homepage = "https://gitlab.com/trebor8/fry-send_tweet.rb"
  spec.licenses = ["BSD (3-Clause)"]
  spec.require_paths = ["lib", "lib/time-ago-in-words/lib"]
  spec.files = Dir[
    "*file",
    "art/*",
    "vms/*",
    "samples/*",
    "*.{txt,gemspec,md,sh}",
    "lib/**/*.{rb,gemspec,md}"
  ]
  spec.add_runtime_dependency "twitter", "~> 6.0"
  spec.add_runtime_dependency "tty-box", "= 0.3.0"
  spec.add_runtime_dependency "unicode-display_width", "~> 1.4"
end

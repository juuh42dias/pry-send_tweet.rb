# fry-send_tweet.rb

* [Introduction](#introduction)
* [API Access](#api-access)
* [Configuration - required](#configuration)
* [Usage](#usage)
  * [Sending Tweets](#sending-tweets)
  * [Reading Tweets](#reading-tweets)
  * [Deleting Tweets](#deleting-tweets)
  * [Liking Tweets](#liking-tweets)
  * [Following / unfollowing](#following-unfollowing)
  * [Searching Twitter](#searching-twitter)
  * [Retweets](#retweets)
  * [Profile Actions](#profile-actions)
  * [Twitter Suggestions](#twitter-suggestions)
  * [Muting other users](#muting-other-users)
  * [Sticky local variable: `_twitter_` ](#special-variable-_twitter_)
  * [Tip: Command Aliases](#tip)
* [Multi-tasking: tmux / screen](#multi-tasking)
* [Install](#install)
  * [Rubygem](#install-rubygem)
  * [Virtual Machines](#vms)
    * [FreeBSD](#vms-freebsd)
* [Screenshots](#screenshots)
* [License](#license)

## <a id='Introduction'>Introduction</a>

An extensible Twitter client that lives inside the Pry and Fry REPLs.

## <a id='api-access'>API Access</a>

Using the Twitter API requires a developer account.
Follow the instructions at [https://developer.twitter.com](https://developer.twitter.com)
if you haven't already setup access to the Twitter API.

## <a id='configuration'>Configuration (required)</a>

The placeholder keys and tokens in the below example can be replaced
by those from your developer account. pry-send_tweet.rb can be configured from
a `.pryrc` file using Ruby, or through a YAML file located at
`$HOME/.pry-send_tweet.yml`

__1. Ruby__

```ruby
# .pryrc
Pry.configure do |config|
  config.twitter = Pry::Config.from_hash({
    # Required
    consumer_key: '<consumer key>',
    consumer_secret: '<consumer secret>',
    access_token: '<access token>',
    access_token_secret: '<access token secret>',
    # Optional
    # The number of tweets to request from Twitter when reading tweets via
    # its API. The default is 200, a lower number could speed up requests if
    # you find them to be slow.
    default_read_size: 100,
    # Optional
    # The User-Agent to use when making requests to the Twitter API.
    # Default is: 'pry-send_tweet.rb vX.Y.Z'.
    user_agent: 'Custom User-Agent',
    # Optional
    # The width of a box that contains a tweet or user. The default width is 100.
    box_width: 120,
    # Optional
    # A Yandex API key for translating tweets when using `read-tweets -x`.
    yandex_key: 'xxx',
    # Optional
    # The language tweets should be translated to, the default is 'en' (English).
    yandex_lang: 'ru'
  })
end
```

__2. YAML__

`$HOME/.pry-send_tweet.yml`:

```yaml
---
# Required
consumer_key: '<consumer key>'
consumer_secret: '<consumer secret>'
access_token: '<access token>'
access_token_secret: '<access token secret>'
# Optional
default_read_size: 100
# Optional
user_agent: 'Your User-Agent string'
# Optional
box_width: 120
# Optional
yandex_key: xxx
yandex_lang: xx
```

## <a id='usage'>Usage</a>

There are four commands added to Pry by this plugin, those commands are:

  * `send-tweet`
    For sending tweets.

  * `read-tweets`
    For reading tweets.

  * `twitter-action` (aliased as: `on-twitter`)
    For performing misc actions on Twitter, such as following, unfollowing, etc.

  * `twitter-search`
    For searching Twitter.

Each command provides help, that can be shown by appending `--help` to the
command. Example:

    [1] pry(main)> send-tweet --help

### <a id='sending-tweets'>Sending tweets</a>


* Send a tweet:

      # Your editor opens (`_pry_.editor`), compose a tweet then hit save & close.
      # You'll then get the url for your tweet:
      [1] pry(main)> send-tweet
      https://twitter.com/xxx/status/xxx


* Send a tweet with an image attached:

      [1] pry(main)> send-tweet --file #{File.join ENV['HOME'], 'photos', 'image.jpg'}


* Send a tweet with multiple images attached:

      [1] pry(main)> send-tweet --file #{%w[1.jpg 2.jpg 3.jpg].join(',')}

* Reply to a tweet:

      [1] pry(main)> send-tweet --reply-to https://twitter.com/username/status/1

* Send a tweet that will self-destruct after 70 seconds.
It's worth adding that if the Pry process exits this operation will be cancelled:

      [1] pry(main)> send-tweet --self-destruct=70

* Send a tweet that will be published 70 seconds in the future.
It's worth adding that if the Pry process exits, this operation will be cancelled:

      [1] pry(main)> send-tweet --delay=70

* Delay sending a tweet until 12AM, today. `--self-destruct` also supports this
  syntax:

      [1] pry(main)> send-tweet --delay=00:00

### <a id='reading-tweets'>Reading tweets</a>

By default the tweets displayed by the `read-tweets` command  are automatically
refreshed every 5 minutes.

That can be changed to a custom interval by setting `Pry.config.twitter.refresh_interval`
to a number of seconds, or to `false` if you want to disable this feature.

* Read recent tweets from your timeline:

      [1] pry(main)> read-tweets

* Read recent tweets from the timeline of another user:

      [1] pry(main)> read-tweets -t rubygems

* Read recent tweets that `@mention` you:

      [1] pry(main)> read-tweets --mentions

* Read recent tweets that you've liked:

      [1] pry(main) read-tweets --likes

* Read recent tweets liked by another user:

      [1] pry(main)> read-tweets --likes ladygaga

* Read recent tweets sent as replies by a given user, the example uses `@github`:

      [1] pry(main)> read-tweets --replies github

* Read a translated copy of a tweet, using the
  [Yandex translation API](https://translate.yandex.com/developers).
  The configuration instructions for Yandex are included in the [configuration section](#configuration).

      [1] pry(main)> read-tweets -x https://twitter.com/user/status/1

* Read a translated copy of a piece of text:

      [1] pry(main)> read-tweets -tx='#{File.binread "persian.txt"}'
      [2] pry(main)> read-tweets -tx='Guten Tag'
      [3] pry(main)> read-tweets -tx='Hola'

* Sometimes Yandex cannot detect the language of the text or tweet being
  translated, or it will guess the source language incorrectly. In case
  this happens, the source language can be set explicitly:

      [1] pry(main)> read-tweets --source-lang=fa -x https://url/to/farsi/tweet
      [1] pry(main)> read-tweets --source-lang=sw -tx='Habari yako'

* Read 100 recent tweets instead of the default 200 recent tweets.
  **Tip:** `--count` can be combined with all of the options described above.

      [1] pry(main)> read-tweets --count 100

* By default retweets are not displayed.
  To display retweets, pass the `--with-retweets` option.

      [1] pry(main)> read-tweets --with-retweets

### <a id='deleting-tweets'>Deleting tweets</a>

* Delete one or more tweets with a comma separated list:

      [1] pry(main)> on-twitter --delete-tweet https://twitter.com/username/status/1,https://twitter.com/username/status/2

* Delete one or more retweets with a comma separated list:

      [1] pry(main)> on-twitter --delete-retweet https://twitter.com/username/status/1,https://twitter.com/username/status/2

### <a id='liking-tweets'>Liking tweets</a>

* Like one or more tweets with a comma separated list:

      [1] pry(main)> on-twitter --like https://twitter.com/user/status/1,https://twitter.com/user/status/2

* Unlike one or more tweets with a comma separated list:

      [1] pry(main)> on-twitter --unlike https://twitter.com/user/status/1,https://twitter.com/user/status/2

### <a id='following-unfollowing'> Following / unfollowing </a>

* Follow one or more tweeters with a comma separated list:

      [1] pry(main)> on-twitter --follow user1,user2,user3

* Unfollow one or more tweeters with a comma separated list:

      [1] pry(main)> on-twitter --unfollow user1,user2,user3,user4

* Show the tweeters who are following you:

      [1] pry(main)> on-twitter --show-followers

* Show the tweeters who you follow:

      [1] pry(main)> on-twitter --show-following

* Both `--show-following` and `--show-followers` accept an optional argument
  that can be used to filter the result set.

      [1] pry(main)> on-twitter --show-following=[0-9]$

### <a id='searching-twitter'>Searching Twitter</a>

The `twitter-search` command can be used to search Twitter.

* Search Twitter using a given hashtag:

      [1] pry(main)> twitter-search #ruby

### <a id='retweets'>Retweets</a>

* Retweet one or more tweets with a comma separated list:

      [1] pry(main)> on-twitter --retweet https://twitter.com/user/status/1,https://twitter.com/user/status/2

### <a id='profile-actions'>Profile actions</a>

* Set the name visible on your profile:

      [1] pry(main)> on-twitter --set-profile-name=<name>

* Set the profile bio / description visible on your profile:

      # An editor opens (`_pry_.editor`), write your bio then hit save & close.
      # Your profiles bio will be updated after closing the editor.
      [1] pry(main)> on-twitter --set-profile-bio

* Set the location visible on your profile:

      # An editor opens (`_pry_.editor`), write a location then hit save & close.
      # Your profiles location will be updated after closing the editor.
      [1] pry(main)> on-twitter --set-profile-location

* Set the color (as a hex value) of links that appear on your profiles timeline:

      [1] pry(main)> on-twitter --set-profile-link-color=#CC0000

* Set the URL visible on your profile:

      [1] pry(main)> on-twitter --set-profile-url=https://github.com/you

* Set profile image:

      [1] pry(main)> on-twitter --set-profile-image /path/to/image.jpg

* Set the banner image visible on your profile:

      [1] pry(main)> on-twitter --set-profile-banner-image /path/to/image.jpg


### <a id='twitter-suggestions'>Twitter Suggestions</a>

* View a list of suggested topics, optionally restricted to Spanish:

      [1] pry(main)> on-twitter --suggested-lang=es --suggested-topics

* View a list of suggested users from a given topic, optionally restricted to
  Spanish:

      [1] pry(main)> on-twitter --suggested-lang=es --suggested-users=<topic>


### <a id='muting-other-users'>Muting other users</a>

* Mute one or more users with a comma separated list:

      [1] pry(main)> on-twitter --mute-user=user1,user2,user3

* Unmute one or more users with a comma separated list:

      [1] pry(main)> on-twitter --unmute-user=user1,user2,user3,user4


### <a id='special-variable-_twitter_'>Sticky local variable: `_twitter_`</a>

The local variable `_twitter_` is inserted into the active Binding, providing
a programmable API accessible to Ruby code as well as a lower level of access to
the Twitter API. It is meant for exploration, development, and debugging.
The local variable is considered reserved.

The local variable returns an instance of `Twitter::REST::Client`:

      [1] pry(main)> _twitter_
      => #<Twitter::REST::Client:0x00007ff44cd6dce0
          @access_token="xxx",
          @access_token_secret="xxx",
          @consumer_key="xxx",
          @consumer_secret="xxx",
          @user_agent="pry-send_tweet.rb v0.7.0">

### <a id='tip'>Tip: Command Aliases</a>

When there are Twitter accounts you read often, it can turn out to be faster
to create command aliases for reading those accounts. An example follows, a
hyphen is included in the aliased commands to avoid collisions with Ruby code.

```ruby
# .pryrc
Pry.commands.alias_command "prez-trump", "read-tweets -t realdonaldtrump"
Pry.commands.alias_command "prez-obama", "read-tweets -t barackobama"
```

## <a id='multi-tasking'>Multi-tasking: tmux / screen</a>

To get the most out of `pry-send_tweet.rb` I use tmux to manage a pane for reading
tweets and another for writing tweets. I've found this to be the best way since
a single shell does not allow for multi tasking but tmux and screen do.

I use [tmuxinator](https://github.com/tmuxinator/tmuxinator) to manage my tmux
sessions. The tmuxinator configuration I use is checked into the repo in the
hope that it might be useful to others.

* [tmuxinator-vagrant.yml](./samples/tmuxinator-vagrant.yml)

## <a id='vms'>Virtual Machines</a>

Before getting started with a Virtual Machine, download and install the
following tools if you didn't already, they're free:

* VirtualBox
* Vagrant

Next, configure access to the Twitter API by adding `.pryrc` file to the root of
the repository. That's covered in the [configuration](#configuration) section.

### Shared space

While exploring, developing or tweeting on one of the available VMs sometimes
you want to bring files from the 'host' machine to the virtual machine, which
is known as the guest machine.

__Syncing files__

`vagrant rsync-auto` can see edits made to the cloned repo on the host
machine and sync those changes back to the guest machine. To set this up, from
the root of the repo on the host machine run:

    $ vagrant rsync-auto

__Tweeting media__

To tweet media such as an image or video from the VM, what I do is drop those
files into `/app/shared-space` and then reference `/app/shared-space` when
sharing them, eg `send-tweet -f /app/shared-space/photo.jpg`.

### <a id='vms-freebsd'>FreeBSD</a>

[FreeBSD 12](https://freebsd.org) Virtual Machine

Vagrant will use Amazon to download the box image. Sometimes the download can be
slow, sometimes the download can stall..

Keep trying if this happens, a download should always resume from the point
where it finished. FreeBSD is worth the perseverance. :)

    $ git clone https://github.com/r-obert/pry-send_tweet.rb
    $ cd pry-send_tweet.rb
    $ ruby vms/freebsd.rb

Screenshots __#1__, __#2__ in the [screenshots](#screenshots) section show the
type of experience to expect when running `ruby vms/freebsd.rb`.

A virtual machine can be destroyed and a new VM instance created by appending
the `--fresh` option:

    $ ruby vms/freebsd.rb --fresh

The default time settings are set to CET (Central European Time). This can be
changed with an environment variable.

    $ VMTZ=Asia/Tehran ruby vms/freebsd.rb

A complete list of time zones are available in the VM:

    $ ls /usr/share/zoneinfo

## <a id='install'>Install</a>

### <a id='install-rubygem'>Rubygem</a>

    $ gem install pry-send_tweet.rb

## <a id='screenshots'>Screenshots</a>

__#1__ Running FreeBSD 12 with Vagrant, Tmux

![FreeBSD-1](/art/44-e44743a5bb.jpg)

__#2__ Composing a tweet on FreeBSD 12 - with Vagrant, Tmux.

![FreeBSD-2](/art/52-eec4df2edf.jpg)

## <a id='license'>License</a>

This project uses the MIT license, see [LICENSE.txt](./LICENSE.txt) for details.

class Pry
  module SendTweet
    #
    # @return [Integer]
    #   The default width of a box that contains a tweet or user.
    #
    # @api private
    #
    DEFAULT_BOX_WIDTH = 100

    #
    # @return [Integer]
    #   The default height of a box that contains a tweet or user.
    #
    DEFAULT_BOX_HEIGHT = 8

    #
    # @return [Integer]
    #   The number of tweets to request when reading tweets from Twitters API.
    #
    # @api private
    #
    DEFAULT_READ_SIZE = 200

    require 'pry' if not defined?(Pry::ClassCommand)
    require 'twitter'
    require 'cgi'
    require 'timeout'
    require 'yaml'
    require 'time-ago-in-words'
    require_relative 'pry/send_tweet/twitter_io'
    require_relative 'pry/pager/system_pager'
    require_relative 'pry/send_tweet/tty-box'
    require_relative 'pry/send_tweet/renderers/tweet_renderer'
    require_relative 'pry/send_tweet/renderers/user_renderer'
    require_relative 'pry/send_tweet/commands/paging/paging_support'
    require_relative 'pry/send_tweet/commands/base_command'
    require_relative 'pry/send_tweet/commands/send_tweet'
    require_relative 'pry/send_tweet/commands/read_tweets'
    require_relative 'pry/send_tweet/commands/twitter_search'
    require_relative 'pry/send_tweet/commands/twitter_action'
    require_relative 'pry/send_tweet/commands/easter_eggs'
    require_relative 'pry/send_tweet/version'

    # @api private
    def self.merge_yaml_file!(config, path)
      if File.readable?(path)
        warn "[warning] Reading pry-send_tweet configuration from '#{path}'."
        twitter_config = YAML.safe_load File.binread(path)
        config.twitter = Pry::Config.from_hash(twitter_config)
      end
    rescue => e
      warn "[warning] Error parsing '#{path}' .. (#{e.class})."
    end
  end
end

Pry.configure do |config|
  b = lambda do |*ary|
    pry = ary[-1]
    twitter = Twitter::REST::Client.new do |config|
      Pry::SendTweet.merge_yaml_file! pry.config,
                                      File.join(ENV['HOME'], '.pry-send_tweet.yml')
      config.consumer_key = pry.config.twitter.consumer_key
      config.consumer_secret = pry.config.twitter.consumer_secret
      config.access_token = pry.config.twitter.access_token
      config.access_token_secret = pry.config.twitter.access_token_secret
    end
    twitter.user_agent = pry.config.twitter.user_agent ||
                         "pry-send_tweet.rb v#{Pry::SendTweet::VERSION}"
    pry.add_sticky_local(:_twitter_) { twitter }
  end
  config.hooks.add_hook(:before_eval, 'pry_send_tweets_before_eval', b)
  config.hooks.add_hook(:before_session, 'pry_send_tweets_before_session', b)
end

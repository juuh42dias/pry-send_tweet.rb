class Pry::Pager::SystemPager
  def pid
    pager.pid
  end

  def fork
    pager
    nil
  end

  def fast_exit!
    Process.kill 'SIGKILL', pid
    Process.wait pid
  end

  private
  # Patch to avoid spawning a shell when launching the pager.
  def pager
    @pager ||= begin
      ary = self.class.default_pager.split(' ')
      io = IO.popen(ary, 'w')
      io.tap{|io| io.sync = true}
    end
  end
end

module Pry::SendTweet::TweetRenderer
  include Timeout
  include TimeAgoInWords

  def render_tweets(tweet_fetcher, title: nil, timeout: _pry_.config.twitter.refresh_interval)
    pager = Pry::Pager::SystemPager.new(_pry_.output).tap(&:fork)
    interval = __choose_render_interval(timeout)
    timeout(interval) do
      began_at, refresh_at = __find_timeout_range(interval)
      rendered_title = __choose_title(title, began_at, refresh_at)
      tweets = __fetch_tweets(tweet_fetcher)
      tweets.empty? ? pager.write("No tweets to show.") :
                      pager.write(__render_tweets(rendered_title, tweets))
    end
  rescue Pry::Pager::StopPaging, Interrupt
    pager.fast_exit!
    system 'reset'
  rescue Timeout::Error
    pager.fast_exit!
    system 'reset'
    retry
  ensure
    pager.close
  end

  private
  # @api private
  def __render_tweets(title, tweets)
    title + tweets.map {|tweet|
      __render_tweet(tweet)
    }.join("\n")
  end

  # @api private
  def __render_tweet(tweet)
    contents = __read_tweet_body(tweet)
    body = "#{tweet.url}\n--\n#{contents}\n"
    height = body.lines.count > box_height ? body.lines.count : box_height
    TTY::Box.frame(height: height,
                   width: box_width,
                   title: {top_left: __render_tweet_title(tweet)}) {body}.to_s
  end

  # @api private
  def __render_tweet_title(tweet)
    user, created_at = tweet.user, tweet.created_at.getlocal
    title = [
      red("@#{user.screen_name}"),
      "Around " + time_ago_in_words(created_at) + " before Last Refresh"
    ].join green(" | ")
    " #{title} "
  end

  # @api private
  def __read_tweet_body(tweet)
    uris = tweet.uris
    text = tweet.attrs[:full_text] ? tweet.attrs[:full_text] :
                                     tweet.full_text
    # 'text' might be a frozen string
    text = text.dup
    uris.each do |uri|
      text.gsub!(uri.attrs[:url], uri.attrs[:expanded_url])
    end
    CGI.unescapeHTML(text).strip
  end

  # @api private
  def __fetch_tweets(tweet_fetcher)
    if tweet_fetcher.respond_to?(:call)
      tweet_fetcher.call
    else
      # Already fetched
      tweet_fetcher
    end
  end

  # @api private
  def __choose_title(title, began_at, refresh_at)
    title = bold green(title || "Twitter")
    timestamps = [bold("Last Refresh: "), began_at.strftime(time_format), "\n"]
    timestamps.concat [
      bold("Next Refresh: "),
      refresh_at.strftime(time_format),
      "\n"
    ] if refresh_at
    title = "#{title}\n\n"
    title += timestamps.join
    title << "\n\n"
    title
  end

  # @api private
  def __choose_render_interval(timeout)
    return nil if timeout == false
    timeout || (60*5)
  end

  # @api private
  def __find_timeout_range(seconds)
    seconds ? [Time.now.getlocal, (Time.now + seconds).getlocal] :
              [Time.now.getlocal, nil]
  end
end

module Pry::SendTweet::UserRenderer
  def render_user(user)
    title = "@#{user.screen_name} ( https://twitter.com/#{user.screen_name} )"
    body = CGI.unescapeHTML <<-USER.each_line.map(&:strip).join("\n")
#{bold("Tweets")}    #{user.tweets_count}
#{bold("Followers")} #{user.followers_count}
#{bold("Following")} #{user.friends_count}
#{user.description}
USER
    height = body.lines.count > box_height ? body.lines.count : box_height
    TTY::Box.frame(
      height: height,
      width: box_width,
      title: {top_left: title}
    ) { body }.to_s
  end
end

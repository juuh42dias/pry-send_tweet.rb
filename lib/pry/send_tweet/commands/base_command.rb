class Pry::Slop
  DEFAULT_OPTIONS.merge!(strict: true)
end

class Pry::SendTweet::BaseCommand < Pry::ClassCommand
  include Pry::SendTweet::TweetRenderer
  include Pry::SendTweet::UserRenderer
  include Pry::SendTweet::PagingSupport

  def self.inherited(kls)
    kls.group 'Twitter'
  end

  def process(*args)
    if nil == _pry_.config.twitter
      raise Pry::CommandError, "_pry_.config.twitter is nil!\n" \
                               "Please set the required keys and tokens to send " \
                               "tweets using Twitters API. \n" \
                               "Visit https://gitlab/trebor8/fry-send_tweet.rb to learn how."
    end
  end

  private

  def default_read_size
    _pry_.config.twitter.default_read_size || Pry::SendTweet::DEFAULT_READ_SIZE
  end

  def box_height
    Pry::SendTweet::DEFAULT_BOX_HEIGHT
  end

  def box_width
    _pry_.config.twitter.box_width || Pry::SendTweet::DEFAULT_BOX_WIDTH
  end

  def search_str_for_users(*ary)
    ary.map do |str|
      if str.start_with?('https://twitter.com')
        path = URI.parse(URI.escape(str)).path
        path.split('/').reject(&:empty?).first
      elsif str.start_with?('@')
        str[1..-1]
      else
        str
      end
    end
  end

  # With thanks to ActionView for this method
  def word_wrap(text, options = {})
   line_width = box_width
   text.split("\n").collect! do |line|
     line.length > line_width ? line.gsub(/(.{1,#{line_width}})(\s+|$)/, "\\1\n").strip : line
   end * "\n"
  end

  def numbered_list(title, ary)
    title = "\n#{bold(title.chomp)}\n\n"
    title + ary.map.with_index { |item, index| yield(item, index+1) }.join("\n")
  end

  def time_format
    "%-d %b %Y, %-I:%M:%S %p"
  end

  def twitter
    @twitter ||= _pry_.binding_stack[-1].eval('_twitter_')
  end
end

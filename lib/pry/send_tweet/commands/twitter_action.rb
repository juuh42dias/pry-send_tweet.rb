
class Pry::SendTweet::TwitterAction < Pry::SendTweet::BaseCommand
  require_relative 'twitter_action/profile_actions'
  require_relative 'twitter_action/suggested_actions'
  require_relative 'twitter_action/like_actions'
  require_relative 'twitter_action/follow_actions'
  require_relative 'twitter_action/mute_actions'
  require_relative 'twitter_action/delete_tweet_actions'
  include ProfileActions
  include SuggestedActions
  include LikeActions
  include FollowActions
  include MuteActions
  include DeleteTweetActions

  match 'twitter-action'
  description 'Like, unlike, follow, unfollow and more.'
  banner <<-B
  twitter-action OPTIONS

  #{description}
  B

  def options(o)
    #
    # Like / unlike tweets
    #
    o.on 'like-tweet=', 'Like one or more tweets', as: Array
    o.on 'unlike-tweet=', 'Unlike one or more tweets', as: Array
    #
    # Following / unfollowing
    #
    o.on 'follow=', 'Follow one or more users', as: Array
    o.on 'unfollow=', 'Unfollow one or more users', as: Array
    o.on 'show-followers=', 'Show the newest users to have followed you or ' \
                            'another user', argument: :optional
    o.on 'show-following', 'Show the newest users you currently follow'
    #
    # Delete tweets and reweets
    #
    o.on 'delete-retweet=', 'Delete one or more retweets', as: Array
    o.on 'delete-tweet=', 'Delete one or more tweets', as: Array
    #
    # Retweet a tweet
    #
    o.on 'retweet=', 'Retweet one or more tweets', as: Array
    #
    # Profile management
    #
    o.on 'set-profile-name=',         'Set the name visible on your profile'
    o.on 'set-profile-bio',           'Set the description visible on your profile'
    o.on 'set-profile-location',      'Set the location visible on your profile'
    o.on 'set-profile-url=',          'Set the URL visible on your profile'
    o.on 'set-profile-banner-image=', 'Set the banner image visible on your profile'
    o.on 'set-profile-image=',        'Set profile photo'
    o.on 'set-profile-link-color=',   'Set the color (as a hex value) of links that ' \
                                      'are visible on your profiles timeline '
    #
    # Suggestions
    #
    o.on 'suggested-topics', 'View topics suggested by Twitter'
    o.on 'suggested-users=', 'A topic from which to view users Twitter ' \
                             'suggests following'
    o.on 'suggested-lang=', 'Restrict suggestion results to a specific language, ' \
                            'given in ISO 639-1 format. Default is "en"'
    #
    # Muting
    #
    o.on 'mute-user=', 'One or more usernames to mute', as: Array
    o.on 'unmute-user=', 'One or more usernames to unmute', as: Array
  end

  def process
    super
    case
    when opts.present?('like-tweet') then like_tweet(opts['like-tweet'])
    when opts.present?('unlike-tweet') then unlike_tweet(opts['unlike-tweet'])
    when opts.present?('follow') then follow_user(opts['follow'])
    when opts.present?('unfollow') then unfollow_user(opts['unfollow'])
    when opts.present?('delete-tweet') then delete_tweet(opts['delete-tweet'])
    when opts.present?('delete-retweet') then delete_retweet(opts['delete-retweet'])
    when opts.present?('retweet') then retweet_tweet(opts['retweet'])
    when opts.present?('set-profile-banner-image') then set_profile_banner_image(opts['set-profile-banner-image'])
    when opts.present?('set-profile-name') then set_profile_name(opts['set-profile-name'])
    when opts.present?('set-profile-image') then set_profile_image(opts['set-profile-image'])
    when opts.present?('set-profile-url') then set_profile_url(opts['set-profile-url'])
    when opts.present?('set-profile-location') then set_profile_location
    when opts.present?('set-profile-link-color') then set_profile_link_color(opts['set-profile-link-color'])
    when opts.present?('set-profile-bio') then set_profile_bio
    when opts.present?('suggested-topics') then suggested_topics
    when opts.present?('suggested-users') then suggested_users(opts['suggested-users'])
    when opts.present?('mute-user') then mute_user(opts['mute-user'])
    when opts.present?('unmute-user') then unmute_user(opts['unmute-user'])
    when opts.present?('show-followers') then show_followers(opts['show-followers'])
    when opts.present?('show-following') then show_following(opts['show-following'])
    end
  end

  private

  def retweet_tweet(tweets)
    retweets = twitter.retweet(tweets)
    if retweets.size > 0
      render_tweets retweets, title: bright_green("Retweeted tweets"), timeout: false
    else
      page_error word_wrap("Nothing retweeted. Are you sure you gave a valid reference to " \
        "one or more tweets, and that you haven't already retweeted the given " \
        "tweet(s) ..?")
    end
  rescue Twitter::Error => e
    page_error word_wrap("Nothing retweeted. Are you sure you gave a valid reference to " \
      "one or more tweets, and that you haven't already retweeted the given " \
      "tweet(s) ..?")
  end

  Pry.commands.add_command(self)
  Pry.commands.alias_command "on-twitter", "twitter-action"
end

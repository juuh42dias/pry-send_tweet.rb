class Pry::SendTweet::ReadTweets < Pry::SendTweet::BaseCommand
  require_relative 'read_tweets/translate_actions'
  include TranslateActions

  match 'read-tweets'
  description 'Read tweets.'
  banner <<-BANNER
  read-tweets [OPTIONS]

  #{description}
  BANNER

  def options(o)
    o.on 't=', 'tweeter=',
         'A username whose timeline you want to read.'
    o.on 'c=', 'count=',
         "The number of tweets to read. The maximum is 200, and the default is " \
         "#{default_read_size}."
    o.on 'l=', 'likes=',
         'Read tweets you or another user have liked.',
         argument: :optional
    o.on 'r=', 'replies=', 'A username whose replies you want to read.'
    o.on 'm', 'mentions', 'Read tweets that @mention you.'
    o.on 'x=', 'translate=', 'Translate a tweet.'
    o.on 'tx=', nil, 'Translate a string of text.'
    o.on 'sl=', 'source-lang=', '[optional] The source language of the ' \
                                'text or tweet being translated'
    o.on 'w', 'with-retweets', 'Include retweets.'
  end

  def process
    super
    case
    when opts.present?('translate') then translate_tweet(opts['x'], opts['sl'])
    when opts.present?('tx') then translate_text(opts['tx'], opts['sl'])
    when opts.present?('replies') then show_replies(user: opts['replies'])
    when opts.present?('likes') then show_likes(user: opts['likes'])
    when opts.present?('mentions') then show_mentions
    else show_tweets_from_timeline(user: opts['tweeter'])
    end
  end

  private

  def show_replies(user:)
    username = search_str_for_users(user).first
    render_tweets lambda {
      twitter.user_timeline(username, timeline_options).select {|tweet|
        tweet.reply? &&
        tweet.in_reply_to_screen_name? &&
        tweet.in_reply_to_screen_name.downcase != username.downcase
      }
    },
    title: "#{'@'+user} replies"
  end

  def show_likes(user:)
    if user
      user = search_str_for_users(user).first
      render_tweets lambda { twitter.favorites(user, count: opts['count'] || default_read_size)},
                    title: "#{'@'+user} likes"
    else
      render_tweets lambda { twitter.favorites(count: opts['count'] || default_read_size) },
                    title: "Your likes"
    end
  end

  def show_tweets_from_timeline(user:)
    if user
      user = search_str_for_users(user).first
      render_tweets lambda { twitter.user_timeline(user, timeline_options) },
                    title: '@'+user
    else
      render_tweets lambda { twitter.home_timeline(timeline_options) },
                    title: "Twitter"
    end
  end

  def show_mentions
    render_tweets lambda { twitter.mentions(timeline_options) },
                  title: "@mentions"
  end

  def timeline_options
    {
      tweet_mode: 'extended',
      include_rts: opts.present?('with-retweets'),
      count: opts.present?('count') ? opts['count'] : default_read_size
    }
  end

  Pry.commands.add_command self
end

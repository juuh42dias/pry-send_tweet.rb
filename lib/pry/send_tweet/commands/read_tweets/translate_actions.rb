module Pry::SendTweet::ReadTweets::TranslateActions
  YANDEX_ENDPOINT = "https://translate.yandex.net/api/v1.5/tr.json/translate?" \
                    "key=%{key}&text=%{text}&lang=%{lang}"

  LANGUAGE_STRINGS = {
    'ar' => 'Arabic (العربية)',
    'en' => 'English',
    'de' => 'German (Deutsch)',
    'pt' => 'Portuguese (Portuguesa)',
    'fa' => 'Farsi (دریافت)',
    'ja' => 'Japanese (日本語)',
    'he' => 'Hebrew (עברית)',
    'ga' => 'Irish (Gaeilge)',
    'es' => 'Spanish (Español)',
    'it' => 'Italinao (italiano)',
    'nl' => 'Dutch (Nederlands)',
    'ru' => 'Russian (русский)',
    'uk' => 'Ukranian (країнська)',
    'ko' => 'Korean (한국어)',
    'fr' => 'French (Français)',
    'da' => 'Danish (dansk)',
    'yi' => 'Yiddish (ייִדיש)',
    'sw' => 'Swahili'
  }

  def translate_tweet(tweet_url, source_language)
    tweet = twitter.status(tweet_url)
    uri_endpoint = __build_yandex_endpoint(tweet, source_language)
    res = Net::HTTP.get_response(uri_endpoint)
    case res
    when Net::HTTPOK
      tweet.attrs[:full_text] = __translated_text_from(res)
      render_tweets [tweet], title: "#{__translation_map(res)}: "
    else
      raise Pry::CommandError, "Bad response from Yandex (#{res.class})"
    end
  end

  def translate_text(text, source_language)
    uri_endpoint = __build_yandex_endpoint(text, source_language)
    res = Net::HTTP.get_response(uri_endpoint)
    case res
    when Net::HTTPOK
      _pry_.output.puts "#{__translation_map(res)}: \n" \
                        "#{__translated_text_from(res)}"
    else
      raise Pry::CommandError, "Bad response from Yandex (#{res.class})"
    end
  end

  private

  # @api private
  def __translation_map(res)
    b = JSON.parse(res.body)
    from, to = b['lang'].split('-')
    from = LANGUAGE_STRINGS[from] || from
    to = LANGUAGE_STRINGS[to] || to
    "#{from} => #{to}"
  end

  # @api private
  def __translated_text_from(res)
    JSON.parse(res.body)["text"][0]
  end

  # @api private
  def __build_yandex_endpoint(tweet, source_language)
    URI.parse format(YANDEX_ENDPOINT,
      lang: URI.encode_www_form_component(__yandex_lang_param(source_language)),
      key: URI.encode_www_form_component(__yandex_key),
      text: URI.encode_www_form_component(
        Twitter::Tweet === tweet ? __read_tweet_body(tweet) : tweet
      )
    )
  end

  # @api private
  def __yandex_lang_param(source_language)
    from = source_language ? "#{source_language}-" : ""
    to = _pry_.config.twitter.yandex_lang || "en"
    from + to
  end

  # @api private
  def __yandex_key
    k = _pry_.config.twitter.yandex_key
    if !k || k.strip.empty?
      raise Pry::CommandError,
            "fatal: _pry_.config.twitter.yandex_key is nil, false or empty"
    end
    k
  end
end

module Pry::SendTweet::TwitterAction::MuteActions
  def mute_user(strings)
    muted = twitter.mute(*search_str_for_users(*strings))
    if muted.size > 0
      page numbered_list("Muted users", muted) {|user, index|
        render_user(user)
      }
    else
      page_error "No one was muted, did you mute those user(s) already ..?"
    end
  rescue Twitter::Error => e
    page_error(e)
  end

  def unmute_user(strings)
    unmuted = twitter.unmute(*search_str_for_users(*strings))
    page numbered_list("Unmuted users", unmuted) {|user, index|
      render_user(user)
    }
  rescue Twitter::Error => e
    page_error(e)
  end
end

module Pry::SendTweet::TwitterAction::SuggestedActions
  def suggested_topics
    suggested_topics = twitter.suggestions lang: suggested_lang
    page numbered_list("Suggested topics: ", suggested_topics) {|topic, index|
      "#{index}. #{topic.slug}"
    }
  end

  def suggested_users(topic)
    topic = URI.escape(topic)
    users = twitter.suggestions(topic, lang: suggested_lang).users
    page numbered_list("Suggested users: ", users) {|user, index|
      render_user(user)
    }
  end

  def suggested_lang
    opts['suggested-lang'] || 'en'
  end
end

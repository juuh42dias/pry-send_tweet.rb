module Pry::SendTweet::TwitterAction::DeleteTweetActions
  def delete_retweet(retweets)
    tweets = twitter.unretweet(retweets)
    if tweets.size == 0
      page_error "Are you sure you gave a valid reference to one or more retweets?"
    else
      render_tweets tweets, title: bold("Deleted retweets"), timeout: false
    end
  rescue Twitter::Error => e
    page_error(e)
  end

  def delete_tweet(tweets)
    tweets = twitter.destroy_status(tweets)
    render_tweets tweets, title: bold("Deleted tweets"), timeout: false
  rescue Twitter::Error => e
    page_error(e)
  end
end

module Pry::SendTweet::TwitterAction::ProfileActions
  def set_profile_url(url)
    if twitter.update_profile url: url
      page_ok "URL updated"
    else
      raise Pry::CommandError, "Something went wrong"
    end
  end

  def set_profile_image(path)
    twitter.update_profile_image(path)
    page_ok "Profile image updated"
  rescue Twitter::Error => e
    page_error(e)
  end

  def set_profile_banner_image(path)
    base64 = Base64.strict_encode64 File.binread(path)
    twitter.update_profile_banner(base64)
    page_ok "Profile banner updated"
  rescue Twitter::Error => e
    page_error(e)
  end

  def set_profile_name(name)
    twitter.update_profile name: name
    page_ok "Name updated"
  rescue Twitter::Error => e
    page_error(e)
  end

  def set_profile_bio
    Tempfile.open('pry-send_tweet-set-profile-bio') do |file|
      Pry::Editor.new(_pry_).invoke_editor(file.path, 0)
      file.rewind
      twitter.update_profile description: file.read
      page_ok "Bio updated"
    end
  rescue Twitter::Error => e
    page_error(e)
  end

  def set_profile_location
    Tempfile.open('pry-send_tweet-set-profile-location') do |file|
      Pry::Editor.new(_pry_).invoke_editor(file.path, 0)
      file.rewind
      twitter.update_profile location: file.read
      page_ok "Location updated"
    end
  rescue Twitter::Error => e
    page_error e
  end

  def set_profile_link_color(color)
    twitter.update_profile profile_link_color: color
    page_ok "Profile link color updated"
  rescue Twitter::Error => e
    page_error e
  end
end

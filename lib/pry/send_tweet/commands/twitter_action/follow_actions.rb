module Pry::SendTweet::TwitterAction::FollowActions
  def follow_user(users)
    users = search_str_for_users(*users)
    follows = twitter.follow(users)
    if follows.size > 0
      page_ok "followed #{join_usernames(follows.map(&:screen_name))}."
    else
      page_error "are you already following #{join_usernames(users)} ..?"
    end
  rescue Twitter::Error => e
    page_error(e)
  end

  def unfollow_user(users)
    users = search_str_for_users(*users)
    unfollows = twitter.unfollow(users)
    if unfollows.size > 0
      page_ok "unfollowed #{join_usernames(unfollows.map(&:screen_name))}."
    else
      page_error "did you already unfollow #{join_usernames(users)} ..?"
    end
  rescue Twitter::Error => e
    page_error(e)
  end

  def show_followers(pattern)
    followers = Array twitter.followers(follow_request_options)
    __follow_filter!(followers, pattern) if pattern
    page numbered_list("Followers", followers) {|follower, index|
      render_user(follower)
    }
  rescue Twitter::Error => e
    page_error(e)
  end

  def show_following(pattern)
    followings = Array twitter.following(follow_request_options)
    __follow_filter!(followings, pattern) if pattern
    page numbered_list("Following", followings) {|following, index|
      render_user(following)
    }
  rescue Twitter::Error => e
    page_error(e)
  end

  private

  # @api private
  def __follow_filter!(users, pattern)
    users.select! do |u|
      u.screen_name =~ /#{pattern}/
    end
  end

  # @api private
  def follow_request_options
    {skip_status: true, include_user_entities: true}
  end

  # @api private
  def join_usernames(users)
    users.map do |username|
      bold("@#{username}") + " ( https://twitter.com/#{username} )"
    end.join(', ')
  end
end

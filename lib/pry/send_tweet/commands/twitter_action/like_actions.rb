module Pry::SendTweet::TwitterAction::LikeActions
  def like_tweet(tweets)
    liked = twitter.favorite(tweets)
    if liked.size > 0
      render_tweets liked, title: bold("Liked tweets"), timeout: false
    else
      page_error "No tweets liked, are you sure you didn't already like those tweet(s) ..?"
    end
  rescue Twitter::Error => e
    page_error(e)
  end

  def unlike_tweet(tweets)
    unliked = twitter.unfavorite(tweets)
    if unliked.size > 0
      render_tweets unliked, title: bold("Unliked tweets"), timeout: false
    else
      page_error "No tweets unliked, are you sure you had liked those tweet(s) ..?"
    end
  rescue Twitter::Error => e
    page_error(e)
  end
end

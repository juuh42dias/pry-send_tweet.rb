class Pry::SendTweet::SendTweet < Pry::SendTweet::BaseCommand
  MAX_TWEET_SIZE = 280

  match 'send-tweet'
  description 'Send a tweet.'
  command_options argument_required: false
  banner <<-BANNER
  send-tweet [options]

  Send a tweet.
  BANNER

  def options(o)
    o.on 'f=', 'file=',
         'One or more paths to file(s) to attach to a tweet.',
          as: Array
    o.on 'r=', 'reply-to=',
         'An absolute url to a tweet you want to reply to.'
    o.on 's=', 'self-destruct=',
         'The number of seconds (represented as a number or a timestamp in the ' \
         'format of HH:MM:SS) to wait before automatically deleting the tweet ' \
         'asynchronously.'
    o.on 'd=', 'delay=',
         'The number of seconds (represented as a number or a timestamp in the ' \
         'format of HH:MM:SS) to wait before creating the tweet asynchronously.'
    o.on 'n', 'no-newline',
         "Remove newlines (\\n) from a tweet before sending it."
  end

  def process(args)
    super
    tweet_creator = create_tweet_creator(compose_tweet_with_editor)
    if opts.present?('delay')
      time_obj, sleep_seconds = parse_duration_str(opts['delay'])
      Thread.new {
        sleep sleep_seconds
        tweet_creator.call
      }
      publish_time = (time_obj ? time_obj : Time.now + sleep_seconds)
                      .getlocal
                      .strftime(time_format)
      page_ok bold("Tweet will be published at approximately #{publish_time}.")
    else
      tweet = tweet_creator.call
      page_ok tweet.url
    end
  end

  private

  def paths_to_twitterio(paths)
    paths.map do |path|
      Pry::SendTweet::TwitterIO.new File.binread(path), File.basename(path)
    end
  end

  def no_newline?
    opts.present?('no-newline')
  end

  def send_tweet_with_media(tweet_contents, medias)
    tweet = twitter.update_with_media(tweet_contents, medias, tweet_options)
    tweet.tap {|t| self_destruct! t.id, opts['self-destruct'] }
  ensure
    medias.each(&:close)
  end

  def send_textual_tweet(tweet_contents)
    tweet = twitter.update(tweet_contents, tweet_options)
    tweet.tap {|t| self_destruct! t.id, opts['self-destruct'] }
  end

  def prepend_username_to_reply!(tweet_url, file)
    tweet = twitter.status(tweet_url)
    mentions = [tweet.user.screen_name].concat tweet.user_mentions.map(&:screen_name)
    file.write mentions.uniq(&:downcase).map{|u| "@#{u}" }.join(' ')
  end

  def replying_to_other_tweet?
    opts.present?('reply-to')
  end

  def tweet_options
    options = {}
    options.merge!({
      in_reply_to_status_id: Integer(File.basename(opts['reply-to']))
    }) if replying_to_other_tweet?
    options
  end

  def compose_tweet_with_editor
    tweet = Tempfile.open('pry-send_tweet--compose-tweet') do |file|
      if replying_to_other_tweet?
        # During experimentation I noticed that without prefixing who we are
        # replying to, the tweet we send will not be considered a reply even
        # with reply_to_status_id being set.
        prepend_username_to_reply!(opts['reply-to'], file)
      end
      file.rewind
      Pry::Editor.new(_pry_).invoke_editor(file.path, 0)
      lines = file.read.each_line
      no_newline? ? lines.map(&:chomp).join(' ') : lines.to_a.join
    end
    validate_tweet!(tweet)
    tweet
  end

  def create_tweet_creator(tweet_contents)
    if opts.present?(:file)
      medias = paths_to_twitterio(opts[:file])
      lambda { send_tweet_with_media(tweet_contents, medias) }
    else
      lambda { send_textual_tweet(tweet_contents) }
    end
  end

  def validate_tweet!(tweet)
    if tweet.strip.empty?
      raise Pry::CommandError, "Can't post an empty tweet."
    elsif tweet.size > MAX_TWEET_SIZE
      raise Pry::CommandError, "The tweet: \n" +
                                word_wrap(tweet) +
                                "\nis too big to publish, try to use less characters."
    end
  end

  def self_destruct!(tweet_id, duration)
    return if !duration
    _, sleep_seconds = parse_duration_str(duration)
    page bold("Tweet due to self destruct in #{sleep_seconds} seconds")
    Thread.new do
      sleep sleep_seconds
      twitter.destroy_status(tweet_id)
    end
  end

  def parse_duration_str(str)
    if str =~ /\A\d+\z/
      sleep_seconds = Integer(str)
    elsif str =~ /\A\d{2}:\d{2}\z/ || str =~ /\A\d{2}:\d{2}:\d{2}\z/
      time_obj = Time.parse(str)
      time_obj += 3600*24 if time_obj <= Time.now
      sleep_seconds = Integer(time_obj - Time.now)
    else
      raise Pry::CommandError, "--delay='#{str}' or --self-destruct='#{str}' is not " \
                               "something I understand."
    end
    [time_obj, sleep_seconds]
  end
  Pry.commands.add_command self
end

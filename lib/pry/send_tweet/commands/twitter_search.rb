class Pry::SendTweet::TwitterSearch < Pry::SendTweet::BaseCommand
  match 'twitter-search'
  description 'Search Twitter.'
  command_options argument_required: true, shellwords: false
  banner <<-BANNER
  twitter-search [options] query

  #{description}
  BANNER

  def options(o)
    o.on 'c=', 'count=', "The number of tweets to show. Default is " \
                         "#{default_read_size}."
  end

  def process(query)
    super
    tweets = filter twitter.search(query, search_options).to_a
    if tweets.empty?
      page "No results to show."
    else
      q = bright_blue(bold(query))
      render_tweets(tweets, title: format("Showing search results for %{q}", q: q))
    end
  end

  private def filter(tweets)
    tweets.reject(&:retweet?)
  end

  private def search_options
    {count: opts['count'] || default_read_size, tweet_mode: 'extended'}
  end

  Pry.commands.add_command self
end

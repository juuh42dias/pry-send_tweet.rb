module Pry::SendTweet::PagingSupport
  def page(str)
    _pry_.pager.page(str.to_s)
  end

  def page_ok(str)
    prefix = bright_green "OK: "
    page "#{prefix}#{str}"
  end
  
  def page_error(str)
    str = str.respond_to?(:message) ? str.message : str
    prefix = bright_red "Error: "
    page "#{prefix}#{str}"
  end
end

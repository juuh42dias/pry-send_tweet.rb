#
# A monkey patch module that implements unicode & emoji support in the gem
# 'tty-box' when using its `frame` method.
#
module Pry::SendTweet::TTYPatch
  require 'tty-box'
  require 'unicode/display_width'
  # Create a frame
  #
  # @api public
  def frame(top: nil, left: nil, width: 35, height: 3, align: :left, padding: 0,
            title: {}, border: :light, style: {})
    output = []
    content = []
    position = top && left

    border = TTY::Box::Border.parse(border)
    top_size    = border.top? ? 1: 0
    bottom_size = border.bottom? ? 1: 0
    left_size   = border.left? ? 1 : 0
    right_size  = border.right ? 1 : 0

    if block_given?
      content = format(yield, width, padding, align)
    end

    fg, bg = *extract_style(style)
    border_fg, border_bg = *extract_style(style[:border] || {})

    if border.top?
      output << cursor.move_to(left, top) if position
      output << top_border(title, width, border, style)
      output << "\n" unless position
    end

    (height - top_size - bottom_size).times do |i|
      output << cursor.move_to(left, top + i + top_size) if position
      if border.left?
        output << border_bg.(border_fg.(pipe_char(border.type)))
      end

      content_size = width - left_size - right_size
      unless content[i].nil?
        output << bg.(fg.(content[i]))
        plain_content = Pry::Helpers::Text.strip_color(content[i])
        content_size -= Unicode::DisplayWidth.of(plain_content, 1, {})
      end
      if style[:fg] || style[:bg] || !position # something to color
        output << bg.(fg.(' ' * content_size))
      end

      if border.right?
        if position
          output << cursor.move_to(left + width - right_size, top + i + top_size)
        end
        output << border_bg.(border_fg.(pipe_char(border.type)))
      end
      output << "\n" unless position
    end

    if border.bottom?
      output << cursor.move_to(left, top + height - bottom_size) if position
      output << bottom_border(title, width, border, style)
      output << "\n" unless position
    end
    output.join
  end
end

module TTY::Box
  prepend Pry::SendTweet::TTYPatch
  singleton_class.module_eval { prepend Pry::SendTweet::TTYPatch }
end

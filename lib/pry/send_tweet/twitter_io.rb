#
# TwitterIO is a child of StringIO that implements an interface compatible
# with the expectations of the Twitter gem. The twitter gem expects a File object
# when uploading media, not an in-memory string as we would like.
#
class Pry::SendTweet::TwitterIO < StringIO
  def initialize(str, basename)
    super(str)
    @basename = basename
  end

  def basename
    @basename
  end

  #
  # For compatibility with File.basename, which attempts String coercion when
  # given an object other than a String.
  #
  # @example
  #   File.basename(twitter_io) => {TwitterIO#basename}
  #
  def to_str
    basename
  end
end

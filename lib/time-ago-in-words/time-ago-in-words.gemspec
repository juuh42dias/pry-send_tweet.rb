# -*- encoding: utf-8 -*-
$:.push File.expand_path("../lib", __FILE__)
require "time-ago-in-words"
Gem::Specification.new do |s|
  s.name        = "time-ago-in-words"
  s.version     = TimeAgoInWords::VERSION
  s.platform    = Gem::Platform::RUBY
  s.authors     = ["Kurtis Rainbolt-Greene", "sirupsen", "r-obert"]
  s.homepage    = "https://github.com/sirupsen/time-ago-in-words"
  s.summary     = %q{Represent a past tense Time object in words}
  s.description = %q{Represent a past tense Time object in words}
  s.files       = Dir["*.{md,txt}", "lib/*.rb", "lib/**/*.rb"]
  s.require_paths = ["lib"]
end

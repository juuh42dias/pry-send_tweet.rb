# time-ago-in-words

## Introduction

Add `time_ago_in_words` method to Time class.

## Credits

This subdirectory is a modified clone of https://github.com/sirupsen/time-ago-in-words.
`time-ago-in-words` is a fork of `time-lord` that focuses on a single feature
instead of many features.

* Credits to author(s) of `time-lord` RubyGem.
* Credits to the author of its fork, `time-ago-in-words`.

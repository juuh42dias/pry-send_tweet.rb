module TimeAgoInWords
  require 'time'
  VERSION = "0.0.5"

  module Units
    Second     = 1
    Minute     = Second  * 60
    Hour       = Minute  * 60
    Day        = Hour    * 24
    Week       = Day     * 7
    Month      = Week    * 4
    Year       = Day     * 365
    Decade     = Year    * 10
    Century    = Decade  * 10
    Millennium = Century * 10
    Eon        = 1.0/0
  end

  module_function
  def time_ago_in_words(time)
    time_difference = Time.now.to_i - time.to_i
    if time_difference <= 0
      return "0 seconds since Last Refresh"
    end
    unit = get_unit(time_difference)
    unit_rep = time_difference > 1 ? "#{unit.to_s.downcase}s" : unit.to_s.downcase
    unit_difference = time_difference / Units.const_get(unit.capitalize)
    "#{unit_difference} #{unit_rep}"
  end

  private
  def get_unit(time_difference)
    Units.constants.each_cons(2) do |con|
      return con.first if (Units.const_get(con[0])...Units.const_get(con[1])) === time_difference
    end
  end
end

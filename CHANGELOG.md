# CHANGELOG

## v1.7.8 (Revenge)

* Add disco biscuit

## v1.7.7 (Revenge)

* Add disco biscuit(s)

* Add warning

## v1.7.6 (Revenge)

* Add disco biscuit

## v1.7.5 (Revenge)

* Add disco biscuit(s)

* Add warning(s)

## v1.7.4 (Revenge)

* Add disco biscuit(s)

## v1.7.3 (Revenge)

* Add disco biscuit(s)

## v1.7.2 (Revenge)

* Add 'The Crew' disco biscuits.

* Update previous ChangeLog row to reference v1.7.1 instead of v1.7.0.

## v1.7.1 (Revenge)

* Add Robert Marley disco biscuits.

* Add Brother Shakur disco biscuits.

## v1.7.0 (Revenge)

* Add the "Trust Nobody" disco biscuit (thanks Brother 2Pac).

* Change codename for the v1.7 series to "Revenge".

## v1.6.5 (Innocent)

* Add warning

## v1.6.4 (Innocent)

* Add disco biscuit(s)

## v1.6.3 (Innocent)

* Add warning, from the Quraysh.

## v1.6.2 (Innocent)

* Add warning

## v1.6.1 (Innocent)

* Carried Howie over to Innocent (where He belongs).

## v1.6.0 (Innocent)

* Add disco biscuit(s)

* Add warning(s)

* Change codename for the v1.6 series to 'Innocent'.

## v1.5.8 (Howie)

* Add disco biscuit(s)

* Add warnings

## v1.5.7 (Howie)

* Add disco biscuit(s)

* Add warnings

## v1.5.6 (Howie)

* Add disco biscuit(s)

* Add warnings

## v1.5.5 (Howie)

* Add disco biscuit(s)

## v1.5.4 (Howie)

* Add disco biscuit(s)

## v1.5.3 (Howie)

* Add disco biscuits.

## v1.5.2 (Howie)

* Add disco biscuits.

## v1.5.1 (Howie)

* Add disco biscuits.

## v1.5.0 (Howie)

* Add disco biscuits.

* Codename for v1.5 series changed to `Howie`.

## v1.4.1 (2020)

* Add disco biscuits.

## v1.4.0 (2020)

* Add warnings.

* Codename for v1.4 series changed to `2020`.

## v1.3.3 (MS-13)

* Special re-release to account for block by USA government.

## v1.3.2 (MS-13)

* Add disco biscuit

## v1.3.1 (MS-13)

* Add disco biscuits

* Update ChangeLog for 1.3.0 (MS-13) release.

## v1.3.0 (MS-13)

* Add concept of a 'CODENAME' (See `version.rb`)

* Add disco biscuits

## v1.2.0

* Fix warning about duplicate disco biscuits.

* Add disco biscuits.

## v1.1.0

* Add disco biscuits.

## v1.0.0

* Read media contents into memory immediately to avoid the potential for
  tampering between the time tweet is scheduled and its actual creation.

* Update `README.md` documentation to include `on-twitter --set-profile-location`.

* Include the time of the last and next refresh cycle when rendering
  tweets that are automatically refreshed every `X` seconds.

* `TimeAgoInWords` represents a past tense Time object as
  `<number> <unit> before <time>`.

* A single instance of `Twitter::REST::Client` shared across potentially
  multiple threads is not going to work, re-create `Twitter::REST::Client`
  instance before each eval, that is local to its Pry instance.

* Switch to `Pry#add_sticky_local` for inserting the special local variable,
 `_twitter_`.

* Add `config.twitter.default_read_size`, for setting the default number of
  tweets to request from Twitter when reading tweets via its API.

## v0.13.1

* Load `$HOME/.pry-send_tweet.yml` using `YAML.safe_load()`.

* Add `time-ago-in-words` README and gemspec to Rubygem package.

## v0.13.0

* Remove `Gemfile`, and the Bundler dependency.

* On the FreeBSD VM, runtime dependencies are installed as FreeBSD packages.

* Rethink fork + wait logic in `read-tweets`.

* Configure the default timezone on the FreeBSD VM to be CET (Central European Time).
  This can be changed by `$VMTZ`. Please see the README for an example.

* `on-twitter --show-following` and `on-twitter --show-followers` accept an
  optional regular expression that can filter through the result set.

* Implement `ruby vms/freebsd.rb --fresh`, for destroying an old VM and
  spinning up a new one.

* Implement `time-ago-in-words` without patching the Time class.

* Remove `spec/`.

* Timestamps in the future or present are represented as '0 seconds' ago
  in `lib/time-ago-in-words`.

* Replace 'time-lord' with 'time-ago-in-words'

* Print an error when unknown switches are passed to command

* Switch default editor to 'ee' on FreeBSD VM

* Add mitigation for RCE bug in apt-get to Dockerfile.

* `--delay`, and `--self-destruct` assume that a time such as 07:00, which  
  has past on the current day, was meant to mean 07:00, tomorrow.

* Install FreeBSD-12 with Vagrant due to unresolvable issues with the
  HardenedBSD vagrant box.  

* Replace `t.co` links with the expanded URL `t.co` redirects to.  

* Collect status of system pager after sending `SIGKILL`.

* When translating text or a tweet, show the source and destination language.  

* Add `read-tweets -sl=`, `read-tweets --source-lang=` for setting the source    
  language of the text or tweet being translated.

* Add `read-tweets -tx`, for translating a piece of text using the Yandex  
  translation API.

* Set CET (Central European Time) as the default timezone for the HardenedBSD VM.

* Implement a minimum box height of 8, but scale to a larger number if a tweet  
  or user bio is written using a lot of newlines.

* Remove the `config.box_height` option.

* Typo fix: rescue `Twitter::Error`, not `Timeout::Error`, when liking a tweet.

## v0.12.1

* Correct multiple grammar errors found in the help output of all commands.

## v0.12.0

* Add `Vagrantfile` & related files to spawn a VirtualBox VM running  
  [HardenedBSD](https://hardenedbsd.org).

* Distribute `samples/*` as part of the Rubygem package.  

## v0.11.0

* Apply a fix to tty-box for the `on-twitter --show-followers`, and  
  `on-twitter --show-following` commands.

* Add unicode support by applying a fix - as a monkey patch - to the `tty-box`  
  gem.

* Add `unicode-display_width`, and `unicode-emoji` as runtime dependencies.  

* Add `read-tweets -x`, `read-tweets --translate` -  for translating tweets,  
  using the Yandex translation API.

* `on-twitter --follow` & `on-twitter --unfollow` understand a string such as  
  `https://twitter.com/StephenKing/media` to mean `@StephenKing`.

* Include a URL to a users profile when following or unfollowing them.

* Remove duplicate username when replying to a tweet. This could happen when the  
  tweet author and the person replying to the tweet are the same.  

* Update `Pry::SendTweet::UserRenderer` to use `config.box_width`, and  
  `config.box_height`.

* Use `page_ok()` from `send-tweet` command.  

## v0.10.1

* Improve forking implementation in `Pry::SendTweet::TweetRenderer`.

* Improve the workaround for weak unicode support in tty-box.

* Add `Pry.config.twitter.box_height` - for defining the height of the box that  
  contains a tweet or user.

* Add `Pry.config.twitter.box_width`, formerly `Pry.config.twitter.line_width`.

## v0.10.0

* Avoid a negative argument error when following / unfollowing other user(s) by  
  bypassing tty-box for those operations.

* Improve the title that appears above tweets when using the `read-tweets`  
  command.  

* Add support for `read-tweets -t https://twitter.com/username`.

* Add `--set-profile-location`, and `--set-profile-link-color=` to the  
 `on-twitter` command.  

## v0.9.0

* Render `Twitter::Tweet` and `Twitter::User` objects using `TTY::Box` - with the  
  knowledge that it has bugs and limitations that will need to be fixed in the   
  future.  

* Improve `send-tweet --reply-to` by writing the tweet owners screen name   
  and all other user mentions in their tweet to the temp file created    
  to compose a reply.  

* Add `on-twitter --show-following` to show the newest tweeters you have started  d
  to follow.

* Add `on-twitter --show-followers` to show the newest tweeters to have followed   
  you.

* Add the option to configure pry-send_tweet through a YAML file.  

* Add `dockerize.sh` to gem package.  

* Add README documentation for following related actions.

* Add README documentation for all `on-twitter` actions related to a users  
  profile.

## v0.8.0ddd

* Add sticky local variable `_twitter_` to the active Binding.

## v0.7.0

* Add `send-tweet -n`, `send-tweet --no-newline`, for removing newlines from a   
  tweet before sending it.

* Add `twitter-action --mute-user=`

* Add `twitter-action --suggested-users=`, `twitter-action --suggested-topics`  
  and `twitter-action --suggested-lang`.

* Add configuration option, `Pry.config.twitter.line_width`, for choosing the  
  a lines width before word wrapping occurs. Default line width is 200.

* Add pre-post tweet validation that catches empty tweets, and tweets who use  
  too many characters.

* Add `send-tweet --delay=xx` for delaying the creation of a tweet by a number  
  of seconds.

* Add `send-tweet --self-destruct=xx` for automatically deleting a tweet after  
  a number of seconds.

* Add `twitter-search`.

* Add `read-tweets --tweeter=`, a longer version of `read-tweets -t`.  

* Add `read-tweets --replies <user>`.

* Add short hand variants for all `read-tweets` options.

* Combine the `read-tweets` options `--my-likes` and `--liked-by` into  
  one option, `--likes`.

* Rename `read-tweets --that-mention-me` to `read-tweets --mentions`.

* Don't hit the Twitter API if a composed tweet is entirely whitespace.

* `send-tweet --file` accepts a comma separated list for attaching multiple  
   images to a tweet.

* Like, unlike, and delete actions accept a comma separated list for performing  
  an action on multiple tweets.

## v0.6.0

* Fix a bug where the auto refresh feature could not be disabled.   
 `Pry.config.twitter.refresh_interval` should be set to `false` instead of  
 `nil` to disable this feature.  

* Modify `render_tweets()` to accept the optional keyword argument, 'timeout'.

* Render liked, unliked, and deleted (re)tweets via `render_tweets()`.

* Change default User-Agent sent with requests to the Twitter API to `pry-send_tweet vX.Y.Z`

* Add the option to configure the `User-Agent` sent with requests to the Twitter API,  
  via `Pry.config.twitter.user_agent`.

* Add `Pry::SendTweet::VERSION`.

## v0.5.3

* Fix another NoMethodError

## v0.5.2

* Fix a NoMethodError when following a user.

* Update gemspec to include all files checked into git.

## v0.5.1

* Update an outdated description for the `read-tweets` command.

* Group the `send-tweet` command under the `Twitter` group, it was previously  
  grouped as '(other)'.

## v0.5.0

* Automatically refresh the tweets on display every 4 minutes.

* `Pry.config.twitter.refresh_interval` can be used for setting a custom refresh  
   interval. The default is 240 seconds, or 4 minutes.

* Add the command `read-tweets`.

* Add the command `twitter-action`, aliased as `on-twitter`.

## v0.4.0

* Add support for attaching an image to a tweet with
  the optional `-f` / `--file` switch:

  `send-tweet --file /path/to/image.jpg`

## v0.3.0

* Compose a tweet using `pry.editor`

## v0.2.1

* Don't escape tweet content using 'Shellwords'.

require 'shellwords'
insert_root_path = ->(tmux_conf) {
  conf = YAML.load(tmux_conf)
  conf['root'] = '/app'
  YAML.dump(conf)
}

Vagrant.configure("2") do |config|
  config.vm.box = "generic/freebsd12"
  config.vm.synced_folder Dir.getwd, "/app", type: "rsync"

  config.vm.provider "virtualbox" do |vb|
    vb.memory = "1024"
  end

  # Install useful packages
  cmds = [
    "sudo pkg install -y zsh tmux",
    "sudo chsh -s zsh vagrant"
  ]
  config.vm.provision "shell", inline: cmds.join(' && ')

  # Configure date & time
  zone = ENV.key?('VMTZ') ? ENV['VMTZ'] : 'CET'
  cmds = [
    "cp /usr/share/zoneinfo/#{zone} /etc/localtime"
  ]
  config.vm.provision "shell", inline: cmds.join(' && ')

  # Insert files into VM
  tmux_conf = insert_root_path.call File.binread('./samples/tmuxinator-vagrant.yml')
  cmds = [
    "echo '#{File.binread('./samples/freebsd-zshrc')}' > /home/vagrant/.zshrc",
    "mkdir -p /home/vagrant/.config/tmuxinator",
    "echo #{Shellwords.shellescape(tmux_conf)} > /home/vagrant/.config/tmuxinator/pry_send_tweet.yml"
  ]
  config.vm.provision "shell", inline: cmds.join(' && ')

  # Install Ruby
  # Install pry-send_tweet dependencies (..as FreeBSD packages)
  cmds = [
    "sudo pkg install -y ruby rubygem-tmuxinator rubygem-pry rubygem-twitter " \
    "rubygem-tty-box rubygem-unicode-display_width"
  ]
  config.vm.provision "shell", inline: cmds.join(' && ')
end

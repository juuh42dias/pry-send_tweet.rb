require 'bundler/gem_tasks'
task :spec do
  Bundler.with_clean_env do
    sh "bundle exec rspec"
  end
end
task default: :spec
